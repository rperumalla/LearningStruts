<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="WEB-INF/lib/css/bootstrap.min.css">
<script src="WEB-INF/lib/js/jquery.min.js"></script>
<script src="WEB-INF/lib/js/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="WEB-INF/lib/js/bootstrap.min.js"></script>

<%@taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
</head>

<body>

	<div class="container">
	<html:errors/>
	<html:errors name="invalieduidorpwd"/>
	<html:form action="loginfrom">
	<table class="table">
	<thead>
		<th colspan="2"> Login Form </th> 
	</thead>	
	<tr>
		<td> <label> User Name :</label> </td>
		<td><html:text property="userName"></html:text></td>
	</tr>
	<tr>
		<td> <label> Password :</label> </td>
		<td><html:password property="password"></html:password></td>
	</tr>
	<tr>		
		<td colspan="2"><html:submit value="submit"></html:submit></td>
	</tr>	
	
	</table>
	
	</html:form>
	 
	
		
	</div>
	
</body>
</html>