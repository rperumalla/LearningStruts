package com.learningstruts.beans;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * Login Form bean.
 * Form bean gives form backup support and validations support for login form.
 * To give backup support and validations support  we need extend ActionForm and overide the method validate.
 * @author rp250054
 */
		
public class LoginForm extends ActionForm{
	
	String userName;
	String password;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	/**
	 * In this validate method you can specify the validations for the login form. 
	 * All the validation messages need to store in ActionsErros Object and return that as shown below.
	 * In struts all the validation messages will take from property file(messages.properties - configured in structs-config.xml) only.
	 */	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		System.out.println(" Validation Started..");
		
		ActionErrors actionErrors = new ActionErrors();
		
		if("".equals(request.getParameter("userName"))) {
			actionErrors.add("username", new ActionMessage("Msg001"));
		}
		
		if("".equals(request.getParameter("password"))) {
			actionErrors.add("password", new ActionMessage("Msg002"));
		}else if(!"admin".equals(request.getParameter("password"))) {
			actionErrors.add("invalieduidorpwd", new ActionMessage("Msg003"));
		}
		System.out.println(" Validation Completed..");
		
		return actionErrors;
		
		
	}

}
