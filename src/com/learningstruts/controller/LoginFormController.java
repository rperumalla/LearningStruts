/**
 * 
 */
package com.learningstruts.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author rp250054
 *
 */
public class LoginFormController extends Action {
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {	
		
		String uname = request.getParameter("userName");
		String password = request.getParameter("password");
		
		System.out.println(" Controller Invoked..");
		
		request.setAttribute("result", uname);
		
		//Mapping contains all the forwards(ActionForward) objects that are specified in struts-config.xml 
		return mapping.findForward("success");
		
	}

}
